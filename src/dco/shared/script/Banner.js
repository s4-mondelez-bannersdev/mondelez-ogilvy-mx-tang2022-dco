//import fitText from '@mediamonks/display-temple/util/fitText';
import untilEnablerIsInitialized from '@mediamonks/display-temple/doubleclick/untilEnablerIsInitialized';
import dataBind from "@mediamonks/display-temple/util/dataBind";
import getEventDispatcher from "@mediamonks/display-temple/doubleclick/getEventDispatcher";
import Events from "@mediamonks/display-temple/doubleclick/Events";
import getDynamicData from "./getDynamicData";
import loadAll from "@mediamonks/display-temple/util/loadAll";


export default class Banner {

  constructor(container, config = null) {
    // add required components here
    this.config = config;
    this.container = container;
  }

  async init() {
    await loadAll([...Object.values(this.config.content.images)]);

    await untilEnablerIsInitialized();
    await this.addEventListeners();

    this.feed = getDynamicData();

    // toString Values for price
    let mainPrice = this.feed._300x250_Price_MainProduct;
    let otherPrice = this.feed._300x250_Price_ComparativeProduct;
    document.querySelector(".product_price").innerHTML = mainPrice.toString();
    document.querySelector(".other_price").innerHTML = otherPrice.toString();

    // values of feed are set on container. with data-bind="src: OBJECT_PATH"
    dataBind(this.feed, this.container);

    window.clickTag = this.feed._300x250_ExitURL.Url;

    // check value of compairson and change green bubble y position

    let compairsonElement = document.querySelector("#comparisonDataHolder");

    switch (this.feed._300x250_Text_ComparativeProduct.trim()) {
      case '2 L':
        console.log('BOTELLA');
        compairsonElement.classList.add("compairson_botella");
        break;
      case '600 ml':
        console.log('PET');
        compairsonElement.classList.add("compairson_pet");
        break;
      case '355 ml':
        console.log('LATA');
        compairsonElement.classList.add("compairson_lata");
        break;
      case '250 ml':
        console.log('TETRA');
        compairsonElement.classList.add("compairson_tetra");
        break;
      default:
        console.log('NO MATCH');
        break;
    }
  }

  addAnimation(animation){
    this.animation = animation;
  }

  async addEventListeners() {
    this.domMainExit = document.body.querySelector('.mainExit');
    this.domMainExit.addEventListener('click', this.handleClick);
    this.domMainExit.addEventListener('mouseover', this.handleRollOver);
    this.domMainExit.addEventListener('mouseout', this.handleRollOut);

    this.dispatcher = await getEventDispatcher();
    //on exit handler
    this.dispatcher.addEventListener(Events.EXIT, this.handleExit);
  }

  handleExit = () => {
    this.animation.getTimeline().progress(1);
  };

  /**
   * When client clicks this function will be triggered.
   */
  handleClick = () => {
    Enabler.exitOverride('Default Exit', this.mainExit);
  };

  /**
   * When mouse rolls over unit.
   */
  handleRollOver = () => {

  };

  /**
   * When mouse rolls out unit.
   */
  handleRollOut = () => {

  };

  async start(){
    await this.init();

    this.animation.play();
  }
}
