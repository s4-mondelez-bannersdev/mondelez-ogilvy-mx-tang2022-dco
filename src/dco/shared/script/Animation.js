import FrameAnimation from "@mediamonks/display-temple/animation/FrameAnimation";

export default class Animation extends FrameAnimation {
  /**
     * @param {HTMLDivElement} container
     * @param {null} config
     */
  constructor(container, config) {
    super();
    this.container = container;
    this.config = config;
    this.dynamicData = null;
  }

  frame0(tl){
    tl.addLabel("frame1")
    tl.to(".content", {duration:.5, opacity: 1, ease:'power4.out'}, "frame1")
    tl.from("#swirlImage", {duration:1, rotation:-20, transform:'center', ease:'power4.out'}, "frame1")
    tl.from("#tangLogo", {duration:1, y:-20, x:20, scale:.5, rotation:-40, transform:'left 45%', ease:'power4.out'}, "frame1")
    if (!this.config.content.isTeHelado){
      tl.from(".droplet_big", {duration:1, y:40, x:-40, scale:.5, rotation:-40, transform:'left 45%', ease:'power4.out'}, "frame1")
      tl.from(".droplet_small", {duration:1, y:80, x:-10, scale:.5, rotation:-20, transform:'left bottom', ease:'power4.out'}, "frame1")
    }
    if(this.config.content.isTeHelado){
      tl.from("#teHeladoCopy", {duration:1, y:20, x:-20, scale:.5, transform:'center', ease:'power4.out'}, "frame1")
    }

  };

  frame1(tl){
    tl.addLabel("frame2", "frame1+=1")
    tl.to("#swirlImage", {duration:.5, rotation:20, opacity:0, transform:'center', ease:'power4.in'}, "frame2")
    tl.to("#tangLogo", {duration:.5, rotation:30, opacity:0, transform:'center 25%', ease:'power4.in'}, "frame2")

    if(this.config.content.isTeHelado){
      tl.to("#teHeladoCopy", {duration:.5, y:-20, x:20, scale:.5, opacity:0, transform:'center', ease:'power4.in'}, "frame2")
    } else {
      tl.to(".droplet_big", {duration:.5, x:40, rotation:35, opacity:0, transform:'center 25%', ease:'power4.in'}, "frame2")
      tl.to(".droplet_small", {duration:.5, x:30, y:-30, rotation:30, opacity:0, transform:'50% center', ease:'power4.in'}, "frame2")
    }

    tl.to("#backgroundImage", {duration:.5, opacity:1, ease:'power4.in'}, "frame2")

    tl.from("#productImage", {duration:1, opacity:0, ease:'power4.out'}, "frame2+=.5")
    tl.from("#comparisonImage", {duration:1, opacity:0, ease:'power4.out'}, "frame2+=.5")
    tl.from(".logo_frame2", {duration:1, rotation:-90, opacity:0, transformOrigin:"center", ease:'power4.out'}, "frame2+=.5")

    if(this.config.content.isTeHelado){
      tl.from("#teHeladoCopyF2", {duration:.5, y:20, x:-20, scale:.5, opacity:0, transform:'center', ease:'power4.out'}, "frame2+=.5")
    }
  
    if (this.config.content.size === "300x250" || this.config.content.size === "250x250"){
      tl.from(".title", {duration:1, x:-300, ease:'power4.out'}, "frame2+=.5")
    } else {
      tl.from(".title", {duration:1, y:-40, opacity:0, ease:'power4.out'}, "frame2+=.5")
    }

  };

  frame2(tl){
    tl.addLabel("frame3", "frame2+=3")

    if (this.config.content.size === "300x250" || this.config.content.size === "250x250" ){
      tl.to(".title", {duration:.5, x:-300, ease:'power4.in'}, "frame3")
    } else {
      tl.to(".title", {duration:.5, y:40, opacity:0, ease:'power4.in'}, "frame3")
    }

    tl.from([".droplet_big_endframe","#productDescription","#productPrice"], {duration:1, y:40, x:-60, scale:.5, rotation:-40, opacity:0, transform:'center', ease:'power4.inOut'}, "frame3")
    tl.from(["#greenDot","#otherDescription","#otherPrice"], {duration:1, y:40, scale:.5, opacity:0, transform:'center', ease:'power4.inOut'}, "frame3+=.5")

    return tl;
  }
  
}
