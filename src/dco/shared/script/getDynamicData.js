export default function getDynamicData() {

  /*****************************************************************************************************************************
   *  Paste DC Dynamic Code HERE                                                                                               *
   *  you can check the example setup here                                                                                     *
   *  https://www.google.com/doubleclick/studio/#campaign:advertiserId=60013113&campaignId=60263003                            *
   *****************************************************************************************************************************/

  // Dynamic Content variables and sample values
  Enabler.setProfileId(10738308);
  var devDynamicContent = {};

  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed = [{}];
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._id = 0;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Unique_ID = 1;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Reporting_Label = "TELIMON_Family-Focus_TetraPak";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Audience = "Family-Focus";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Start_Date = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Start_Date.RawValue = "5/1/2022 0:00:00";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Start_Date.UtcValue = 1651388400000;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].End_Date = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].End_Date.RawValue = "5/31/2022 23:59:59";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].End_Date.UtcValue = 1654066799000;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Template_Complexity = "Medium";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Background_Image = "Lifestyle";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Copy = "Medium-neutral";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Product_Image = "Product in-use";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].CTA_Copy = "N\/A";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].Audience_Name = "Family-Focus";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_Headline = "Rinde para relajarse juntos";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_Text_MainProduct = "2 LITROS";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_Price_MainProduct = 5;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_MainProduct = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_MainProduct.Type = "file";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_MainProduct.Url = "https://s0.2mdn.net/ads/richmedia/studio/60005443/60005443_20220412094210016_product_te_300x250.png";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_Text_ComparativeProduct = "250 ml ";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_Price_ComparativeProduct = 7;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_ComparativeProduct = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_ComparativeProduct.Type = "file";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_ComparativeProduct.Url = "https://s0.2mdn.net/ads/richmedia/studio/60005443/60005443_20220411062907516_vs_tetra_300x250.png";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_CTA = "";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_ExitURL = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x250_ExitURL.Url = "https://super.walmart.com.mx/browse/mundo-mondelez/bebidas-polvo?utm_source=mtz_mondelez&utm_medium=mtz_digital&utm_campaign=mtz_mondelez_lf_tang_20220301&utm_content=banner&utm_term=";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_Headline = "Rinde para relajarse juntos";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_Text_MainProduct = "2 LITROS";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_Price_MainProduct = 5;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_MainProduct = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_MainProduct.Type = "file";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_MainProduct.Url = "https://s0.2mdn.net/ads/richmedia/studio/60005443/60005443_20220412094216660_product_te_300x600.png";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_Text_ComparativeProduct = "250 ml ";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_Price_ComparativeProduct = 7;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_ComparativeProduct = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_ComparativeProduct.Type = "file";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_ComparativeProduct.Url = "https://s0.2mdn.net/ads/richmedia/studio/60005443/60005443_20220411062859719_vs_tetra_300x600.png";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_CTA = "";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_ExitURL = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._300x600_ExitURL.Url = "https://super.walmart.com.mx/browse/mundo-mondelez/bebidas-polvo?utm_source=mtz_mondelez&utm_medium=mtz_digital&utm_campaign=mtz_mondelez_lf_tang_20220301&utm_content=banner&utm_term=";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_Headline = "Rinde para relajarse juntos";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_Text_MainProduct = "2 LITROS";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_Price_MainProduct = 5;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_MainProduct = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_MainProduct.Type = "file";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_MainProduct.Url = "https://s0.2mdn.net/ads/richmedia/studio/60005443/60005443_20220412094205133_product_te_250x250.png";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_Text_ComparativeProduct = "250 ml ";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_Price_ComparativeProduct = 7;
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_ComparativeProduct = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_ComparativeProduct.Type = "file";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_ComparativeProduct.Url = "https://s0.2mdn.net/ads/richmedia/studio/60005443/60005443_20220411062822825_vs_tetra_250x250.png";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_CTA = "";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_ExitURL = {};
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0]._250x250_ExitURL.Url = "https://super.walmart.com.mx/browse/mundo-mondelez/bebidas-polvo?utm_source=mtz_mondelez&utm_medium=mtz_digital&utm_campaign=mtz_mondelez_lf_tang_20220301&utm_content=banner&utm_term=";
  devDynamicContent.MM_MDLZ_Tang2022_DCO_Feed[0].ACTIVE = true;
  Enabler.setDevDynamicContent(devDynamicContent);



  /*****************************************************************************************************************************
   *  END Paste DC Dynamic Code                                                                                                *
   *****************************************************************************************************************************/

  let feedName = Object.keys(devDynamicContent)[0];

  return window.dynamicContent[feedName][0];
}
